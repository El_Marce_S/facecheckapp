package com.example.facecheckin;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class CameraActivity extends AppCompatActivity {
    Activity activity;

    final Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            final String Cedula = sp.getString("Cedula", "NOData");
            File picture_file = getOutputMediaFile();
            if (picture_file == null) {
                return;
            } else {
                try {
                    FileOutputStream fos = new FileOutputStream(picture_file);
                    fos.write(data);
                    fos.close();
                    DoRequest(picture_file);


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        public File getOutputMediaFile() {
            String state = Environment.getExternalStorageState();
            if (!state.equals(Environment.MEDIA_MOUNTED)) {
                return null;
            } else {
                File folder_gui = new File(Environment.getExternalStorageDirectory() + File.separator + "GUI");
                if (!folder_gui.exists()) {
                    folder_gui.mkdirs();
                }
                File outputFile = new File(folder_gui, "temp.jpg");
                return outputFile;
            }
        }
    };
    Camera camera;
    FrameLayout frameLayout;
    ShowCamera showCamera;
    OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .build();

    @Override
    public boolean moveSharedPreferencesFrom(Context sourceContext, String name) {
        return super.moveSharedPreferencesFrom(sourceContext, name);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        Log.d("myTag", "Entra en la Actividad On Create");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);

        camera = Camera.open(1);
        showCamera = new ShowCamera(this, camera);
        frameLayout.addView(showCamera);

    }

    private void DoRequest(final File file) {
        final String[] mensajes = new String[1];
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    final String Cedula = sp.getString("Cedula", "NOData");
                    final String TipoCheck = sp.getString("TipoCheck", "NOData");
                    OkHttpClient client = new OkHttpClient();
                    Log.d("myTag", "Entra Al Request");
                    Log.d("myTag", TipoCheck);
                    RequestBody requestBody = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("fileToCompare01", file.getName(),
                                    RequestBody.create(MediaType.parse("image/jpg"), file))
                            .addFormDataPart("CI", Cedula)
                            .addFormDataPart("CheckType", TipoCheck)
                            .build();
                    Request request = new Request.Builder()
                            .url("http://www.techsquad.xyz/FaceChekIn/index.php/FaceCheckApi/Compare")
                            .post(requestBody)
                            .build();

                    Response result = client.newCall(request).execute();
                    if (!result.isSuccessful()) {
                        Log.d(" Error: ", result.message());
                    } else {

                        String cadena[] = {"<h1>El Reconocimiento ha sido </h1>Positivo"};
                        boolean resultado = result.body().string().contains(cadena[0]);
                        if (resultado) {
                            runOnUiThread(new Runnable() {
                                public void run() {


                                    Toast.makeText(getBaseContext(), "Checkin correcto", Toast.LENGTH_LONG).show();


                                    finishAffinity();
                                }
                            });
                        } else {
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(getBaseContext(), "Checkin errado", Toast.LENGTH_LONG).show();
                                    finishAffinity();
                                }
                            });
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("myTag", "Error");
                }
            }
        }).start();
    }

    public void CaptureImage(View v) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor edit = sp.edit();
        edit.putString("TipoCheck", "0");
        edit.commit();

        if (camera != null) {
            camera.takePicture(null, null, mPictureCallback);
        }
    }
    public void CaptureImage2(View v) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor edit = sp.edit();
        edit.putString("TipoCheck", "1");
        edit.commit();

        if (camera != null) {
            camera.takePicture(null, null, mPictureCallback);
        }
    }

}