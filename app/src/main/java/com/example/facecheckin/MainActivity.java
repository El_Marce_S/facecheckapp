package com.example.facecheckin;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_CODE = 200;
    EditText etCedula;
    Button saveCedula, ShowData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etCedula = findViewById(R.id.etCedula);
        saveCedula = findViewById(R.id.saveCedula);

        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);


        saveCedula.setOnClickListener(v -> {
            if (!checkPermission() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermission();
            } else {
                SharedPreferences.Editor edit = sp.edit();
                edit.putString("Cedula", etCedula.getText().toString());
                edit.putInt("Registrado", 1);
                edit.commit();
                Intent intent = new Intent(getApplicationContext(), CameraActivity.class);
                startActivity(intent);
            }
        });


    }

    private boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int result12 = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return result1 == PackageManager.PERMISSION_GRANTED && result12 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{CAMERA, WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepteds = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (cameraAccepted && cameraAccepteds) {
                        Toast.makeText(getApplicationContext(), "Permisos aceptados", Toast.LENGTH_SHORT).show();
                    } else {

                        Toast.makeText(getApplicationContext(), "Faltan permisos por conceder", Toast.LENGTH_SHORT).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA) && shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
                                RequestPermission("La aplicación necesita hacer uso del permiso para ,usar la Cámara¿Desea conceder este permiso?",
                                        (dialog, which) -> requestPermissions(new String[]{CAMERA, WRITE_EXTERNAL_STORAGE},
                                                PERMISSION_REQUEST_CODE));
                                return;
                            }
                        }

                    }
                }


                break;
        }
    }

    public void RequestPermission(String msj, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(msj)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancelar", null)
                .create()
                .show();
    }
}
